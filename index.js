db.fruits.aggregate({ $count: "stock" });

db.fruits.aggregate([{ $match: { stock: { $gt: 20 } } }, { $count: "stock" }]);

db.fruits.aggregate([
  { $group: { _id: "$supplier_id", total: { $avg: "$stock" } } },
  { $project: { _id: 0 } },
]);

db.fruits.aggregate([
  { $group: { _id: "$supplier_id", total: { $avg: "$stock" } } },
]);

db.fruits.aggregate([
  { $group: { _id: "$supplier_id", total: { $max: "$stock" } } },
]);

db.fruits.aggregate([
  { $group: { _id: "$supplier_id", total: { $min: "$stock" } } },
]);
